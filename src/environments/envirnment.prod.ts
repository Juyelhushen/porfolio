export const environment = {
  production: true,
  // socialLinks

  fbLink: 'https://www.facebook.com/mahammad.juyel.1/',
  instaLink: 'https://www.instagram.com/iamjuyel/',
  youtubeLink: 'https://www.youtube.com/channel/UCKuZogE-0ye5wpY0_JMdcIg',
  linkedinLink: 'https://www.linkedin.com/in/juyel-hushen-770674170/',
  githubLink: 'https://github.com/juyelhushen',

  // email
  mailService: 'service_groma9h',
  templateId: 'template_94r6juq',
  emailJSKey: '_wNNTpyuKCY1QjQGI',

  // project

  finmart: 'https://finmart.ae/',
  corban: 'http://13.232.170.246:4500/',
  biriyumee:
    'https://personal-hekanzay.outsystemscloud.com/BiriyaniExchange_ui/login',
  joefolio: 'https://github.com/juyelhushen/Joefolio.git',
};
